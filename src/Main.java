import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        intro();

        //System.out.println();
        int number = userInput.nextInt();
        //String name = userInput.nextLine();

        if (number == 1) {
            System.out.println("You approach the cave...");
            System.out.println("It is dark and spooky");
        } else {
            System.out.println("A large dragon jumps out in front of you! He opens his jaws and...");
            System.out.println("Gobbles you  down in one bite");
        }
    }


    public static void intro() {
        System.out.println("You are in the land full of dragons. In front of you,");
        System.out.println("you see two caves. In one cave, the dragon is friendly");
        System.out.println("and will share his treasure with you. The other dragon");
        System.out.println("is greedy and hungry and will eat you on sight.");
        System.out.println("\nWhich cave will you go into? (1 or 2)"); // next line
    }
}

